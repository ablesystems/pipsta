[![Pipsta Logo](https://bitbucket.org/repo/enq8r6/images/3048173118-Logo_small.jpg)](http://www.pipsta.co.uk)

# Websites #
Pipsta Website: http://www.pipsta.co.uk

Android NFC App: https://play.google.com/store/apps/details?id=com.pipsta.pipstanfcprinter

# Example Applications #

Pipsta is at the heart of all sorts of [applications](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Projects)!

# Where to Start #

* Buy Pipsta [here](https://bitbucket.org/ablesystems/pipsta/wiki/Buy%20Pipsta)
* [Assemble the Pipsta](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20Assembly%20Instructions)
* [Install Raspbian and Pipsta Software on the Raspberry Pi](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20First-Time%20Setup)
* Browse the [Tutorials Index](https://bitbucket.org/ablesystems/pipsta/wiki/Tutorials)



# What's New #

* **06/10/2017** - Updated scripts to support new (and old) USB PID's. Updated firmware to V9.2.09 (fix for double height text rendering).
* **06/06/17** - Updated [CUPS driver](https://bitbucket.org/ablesystems/pipsta/wiki/Printing%20Using%20CUPS)
* **22/12/16** - New [CUPS driver](https://bitbucket.org/ablesystems/pipsta/wiki/Printing%20Using%20CUPS) (with [installer](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20and%20CUPS%20Package%20Installation)) and Pipsta [installer](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20and%20CUPS%20Package%20Installation).
* **01/06/16** - We are in the process of releasing the Full CUPS driver to our Beta test team. Upon final release, this will allow native printing from any Raspbian application!
* **26/05/16** - We've now released the Android NFC Application source code [here](https://bitbucket.org/ablesystems/nfcdemoapp) for you to design your very own Pipsta apps. [Let us know](mailto:support@pipsta.co.uk) what you get up to!
* **21/04/16** - Improvements to flow of assembly and installation aspects of the wiki for newcomers, Pi3B compatibility made more prominent, Jessie emphasised over Wheezy.
* **29/02/16** - Our Pi Zero compatible base-plates have arrived and work well. This will initially be introduced as a Pi Zero kit. Please [contact us](mailto:support@pipsta.co.uk) if you would like more information.
* **24/02/16** - A new [tech bulletin](https://bitbucket.org/ablesystems/pipsta/wiki/Printing%20to%202%20Pipstas%20from%201%20Pi) has been added addressing requests to print to two Pipsta printers from a single Raspberry Pi.

# What's Next #

Update examples to use new Pipsta USB ID (removing current installation instruction to modify examples).  Improvements to CUPS driver for Pipsta. Improved package based distributions.

# README #

The python scripts and the documentation in this repository have been provided
to demonstrate how the Pipsta can be used in an educational or hobbyist
environment.  The Pipsta is a based around a
[Raspberry Pi](http://www.raspberrypi.org/) and a thermal printer.

### What is this repository for? ###

* Demonstration of the Pipsta
* Documentation of the setup of the Pipsta
* Documentation of the examples
* Proposed projects
* Beta
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Diagnosing Basic Printing Problems ###
**NEW** A script (`verify_pipsta_install.py`) has been added to the root of the project to try and help diagnose any installation issues.  The script checks OS, python and python libraries then it attempts to communicate with the printer.  If you would like any changes to the script then please feel free to send a request (or a patch) to [support@pipsta.co.uk](mailto:support@pipsta.co.uk).

~~~
python verify_pipsta_install.py
~~~

| Problem                                  | Possible Solution                        |
| ---------------------------------------- | ---------------------------------------- |
| Printer LED not illuminated              | Check both the power to the printer (on the back of the Pipsta) and the USB connection from Raspberry Pi to printer |
| Printer LED flashing green               | Ensure the paper is loaded correctly     |
| Printer LED flashing green-off-red-off   | Ensure the printer power supply is present. Whilst the Raspberry Pi and Printer can communicate with just a USB connection, printing cannot take place without the printer power being applied |
| Permission error when running python script | Ensure you copied the system files to the correct locations by opening LXTerminal and pressing [UP ARROW] on the keyboard to review the previous terminal commands. |
| IO Error: Printer not found              | Enter `ls /dev/a*` at the command line to list connected devices beginning with ‘a’. If you do not see ‘Ap1400’ listed, Linux cannot see the printer. Manually check that printer USB connectors are located correctly at both ends. |
| Issue not resolved by above checks       | Remove the USB connection to the printer, wait a few seconds, then replace |
| Issue not resolved by above checks       | Shut-down the Pi and remove power from both the Raspberry Pi and the printer. Reconnect power to both and wait for the unit to reboot |
| Issue not resolved by above checks       | Send an email to <support@pipsta.co.uk>  |

### Shutting Pipsta Down Safely ###
Whilst the printer is resilient when it comes to powering down, the Raspberry Pi must undergo a strict shutdown process to avoid corrupting the Micro SD card. 

* The most straightforward method of doing this is to double-click the ‘Shutdown’ icon on the desktop.
* If you are already in LXTerminal, type `$ sudo shutdown –h now` to shut-down the Raspberry Pi immediately.

**Always make sure ALL activity on the Raspberry Pi’s green LED (the LED on the right) has stopped before removing the power!**

### Upgrading the pipsta Firmware ###

:warning:The fpu does not yet work with Rasbpian stretch.  A patch is in progress.

A new tool (the fpu) has been created to allow pipsta firmware to be installed from Linux.  This has been packaged up for Raspbian and placed in the download page of this bitbucket site (along with the new firmware).  To install the new firmware follow the instructions below.

1. Download [pipsta-printer-utilities-1.1.1-Linux.deb](https://bitbucket.org/ablesystems/pipsta/downloads/pipsta-printer-utilities-1.1.1-Linux.deb) and [V9.2.09.able](https://bitbucket.org/ablesystems/pipsta/downloads/V9_2_09.able) to your Raspberry Pi.
2. Install the printer utilities by running the following command      
   ```bash
    $ sudo apt-get update
    $ sudo dpkg -i pipsta-printer-utilities-1.1.1-Linux.deb
    $ sudo apt-get -f install
   ```

3. Check the install `$ fpu --version`
4. Install the new firmware `$ fpu V9.2.09.able`

If you have any problems please contact us.

### Who do I talk to? ###
* [support@pipsta.co.uk](mailto:support@pipsta.co.uk)

### Why Not Visit ###
* [Facebook](https://www.facebook.com/pages/Pipsta/921416174536872)
* [Twitter](https://twitter.com/PipstaPrinter)
* [Youtube](https://www.youtube.com/channel/UCPkYuupnqoPXgz6yDQcf0nQ)