# verify_pipsta_install.py
# Copyright (c) 2014,2016 Able Systems Limited. All rights reserved.
import platform
import sys
import socket
import fcntl
import struct
import array
import os.path
import subprocess

FEED_PAST_CUTTER = b'\n' * 5

PIPSTA_USB_VENDOR_ID = 0x0483
PIPSTA_USB_PRODUCT_ID = 0xA19D
AP1400_USB_PRODUCT_ID = 0xA053
AP1400V_USB_PRODUCT_ID = 0xA19C

valid_usb_ids = {PIPSTA_USB_PRODUCT_ID, AP1400_USB_PRODUCT_ID, AP1400V_USB_PRODUCT_ID}

def get_ip_address(ifname):
    '''
    The following function is used to obtain the IP address of the Pipsta's
    RaspberryPi. This code has been taken from
    http://stackoverflow.com/questions/24196932
    '''
    siocgifaddr = 0x8915
    tmp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(
        fcntl.ioctl(
            tmp_socket.fileno(), siocgifaddr, struct.pack('256s', ifname[:15])
        )[20:24]
    )

def all_interfaces():
    '''
    Returns a list of all the network interfaces found on the Pipsta's
    RaspberryPi. The code is from http://code.activestate.com/recipes/439093/
    '''
    max_possible = 128  # arbitrary. raise if needed.
    max_byte_count = max_possible * 32
    tmp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    names = array.array('B', '\0' * max_byte_count)
    siocgifconf = 0x8912
    outbytes = struct.unpack(
        'iL', fcntl.ioctl(
            tmp_socket.fileno(), siocgifconf,
            struct.pack('iL', max_byte_count, names.buffer_info()[0])
        )
    )[0]
    namestr = names.tostring()
    return [namestr[i:i+32].split('\0', 1)[0] for i in range(0, outbytes, 32)]

def module_exists(module_name):
    '''
    Attempts to import the named module if, returns True on success else False
    '''
    try:
        __import__(module_name)
    except ImportError:
        return False

    return True

PLATFORM_UNKNOWN = None
PLATFORM_DEBIAN_BUZZ = int(float('1.1'))
PLATFORM_DEBIAN_REX = int(float('1.2'))
PLATFORM_DEBIAN_BO = int(float('1.3'))
PLATFORM_DEBIAN_HAMM = int(float('2.0'))
PLATFORM_DEBIAN_SLINK = int(float('2.1'))
PLATFORM_DEBIAN_POTATO = int(float('2.2'))
PLATFORM_DEBIAN_WOODY = int(float('3.0'))
PLATFORM_DEBIAN_SARGE = int(float('3.1'))
PLATFORM_DEBIAN_ETCH = int(float('4.0'))
PLATFORM_DEBIAN_LENNY = int(float('5.0'))
PLATFORM_DEBIAN_SQUEEZE = int(float('6.0'))
PLATFORM_DEBIAN_WHEEZY = int(float('7.0'))
PLATFORM_DEBIAN_JESSIE = int(float('8.0'))

def os_check():
    '''
    Checks that the platform this is installed on is a Debian Linux built for
    the arm platform.  The Debian release must be Wheezy or Jessie.
    '''
    if platform.system() != 'Linux':
        print platform.system() + ' is not yet supported by Pipsta'
        return PLATFORM_UNKNOWN

    if not platform.machine().startswith('arm'):
        machine = platform.machine()
        print 'Hardware of type "' + machine + ' is not yet supported by Pipsta'
        return PLATFORM_UNKNOWN

    dist = platform.linux_distribution()
    if dist[0] != 'debian':
        print dist[0] + ' is not yet supported by Pipsta'
        return PLATFORM_UNKNOWN

    # We can safely test an int as the decimal part of the number directly maps
    # to the release since 'etch' and that's old.
    major_rel_id = int(float(dist[1]))
    if major_rel_id < PLATFORM_DEBIAN_WHEEZY:
        print 'Releases older than wheezy are not yet supported by Pipsta.'
        print 'Continuing to test as though a wheezy release in the vague '\
            'hode it will work.'

        return PLATFORM_DEBIAN_WHEEZY

    if major_rel_id > PLATFORM_DEBIAN_JESSIE:
        print 'Releases newer than jessie are not yet supported by Pipsta.'
        print \
            'Continuing to test as though a jessie release in the vague hope ' \
            'it will work.'
        return PLATFORM_DEBIAN_JESSIE

    return major_rel_id

def python_check():
    '''
     Verifies the python flavour is a valid version of python2
    '''
    python_ver_ok = (sys.version_info > (2, 6) and sys.version_info < (3, 0))

    if not python_ver_ok:
        print 'The demoes are targeted at python2 versions 2.6 and above'

    return python_ver_ok

def pyusb_check():
    '''Verifies that a suitable version of pyusb is installed'''
    pyusb_ok = module_exists('usb') == True and \
        module_exists('usb.core') == True  and module_exists('usb.util') == True

    if not pyusb_ok:
        print 'The Pipsta is connected over USB, the demoes all depend on ' \
              'the pyusb module'

    return pyusb_ok

def pillow_check():
    '''Verifies the correct version of PIL (Pillow) is installed'''
    if not module_exists('PIL.Image'):
        print('The Pillow library for python is not installed and is required '
              'by most of the demoes')
        return False

    return True

class printer_finder(object):
    def __call__(self, device):
        if device.idVendor != PIPSTA_USB_VENDOR_ID:
            return False

        return True if device.idProduct in valid_usb_ids else False

def usb_enumeration_check():
    '''
    Checks the printer can be enumerated (the USB connection is correctly
    installed)
    '''
    import usb.core
    dev = usb.core.find(custom_match=printer_finder())
    assert dev, 'Printer failed to enumerate'
    return dev

def pip_check():
    '''Checks the pip installer for python modules is installed'''
    return module_exists('pip')

def wait_for_printer(dev):
    '''
    Waits until communication with the printer can be established.  If only
    partial communications are established the user is provided hints to make
    the printer fully operational.
    '''
    status = struct.unpack(
        'B', dev.ctrl_transfer(0xC0, 0x0d, 0x0200, 0, 1)
    )[0]
    while status & 0x40 == 0x40:
        print '''Printer is in error state can you check the following -
  Paper is installed
  Printers power supply is connected
  The LED on the front of the panel is a constant green'''
        raw_input('Press return to continue ')
        status = struct.unpack(
            'B', dev.ctrl_transfer(0xC0, 0x0d, 0x0200, 0, 1)
        )[0]

def get_default_interface(dev, cfg):
    '''
        Looks for the default interface on the printers current USB
        configuration
    '''
    import usb.core
    interface_number = cfg[(0, 0)].bInterfaceNumber
    usb.util.claim_interface(dev, interface_number)
    alternate_setting = usb.control.get_interface(dev, interface_number)
    interface = usb.util.find_descriptor(
        cfg, bInterfaceNumber=interface_number,
        bAlternateSetting=alternate_setting)
    return interface

def check_bulk_read(interface):
    '''Check the bulk read endpoint is valid'''
    import usb.core
    usb_in = usb.util.find_descriptor(
        interface,
        custom_match=lambda e:
        usb.util.endpoint_direction(e.bEndpointAddress) ==
        usb.util.ENDPOINT_IN
    )
    assert usb_in, 'No bulk in endpoint found for printer'
    from usb.core import USBError
    try:
        junk = usb_in.read(1)
        while junk:
            print junk
    except USBError as dummy:
        pass

    return usb_in

def check_bulk_write(interface):
    '''Check the bulk write endpoint is correct'''
    import usb.core
    usb_out = usb.util.find_descriptor(
        interface,
        custom_match=lambda e:
        usb.util.endpoint_direction(e.bEndpointAddress) ==
        usb.util.ENDPOINT_OUT
    )
    assert usb_out, 'No bulk out endpoint found for printer'
    return usb_out

def ok_if_found(found):
    '''Utility method returns OK if True else False'''
    return 'Ok' if found else 'Missing'

def configure_usb_connection(dev):
    '''Gets a connection to the printer'''
    dev.reset()
    dev.set_configuration()
    cfg = dev.get_active_configuration()
    assert cfg, 'Failed to find an active configuration for the printer'
    return cfg

def check_printer_communications(usb_out, usb_in):
    '''Checks bi-direction communications with the printer can be established'''
    usb_out.write(b'\x1dI\x06')
    printer_id = usb_in.read(9)
    assert printer_id and printer_id != '', \
        'Printer did not respond with a valid ID'

    print 'Successful bi-directional bulk communications established with ' \
          'printer'
    return ''.join([chr(c) for c in printer_id]).strip()

def get_dist_info_string():
    '''Returns a string describing the Linux distribution, suitable for printing
    on Pipsta.'''
    (distname, linux_version, dummy) = platform.linux_distribution()
    return 'Running on {} V{}\n'.format(distname, linux_version)

def check_usb_connection():
    '''Attempt to get a connection to the printer'''
    dev = usb_enumeration_check()
    cfg = configure_usb_connection(dev)

    # Check printer is powered
    wait_for_printer(dev)
    interface = get_default_interface(dev, cfg)

    usb_in = check_bulk_read(interface)
    usb_out = check_bulk_write(interface)

    # Check full bulk communications
    printer_id = check_printer_communications(usb_out, usb_in)
    return usb_out, usb_in, printer_id

def print_host_info(usb_out):
    '''Print information about the host operating system'''
    usb_out.write(get_dist_info_string())
    (system, node, release, dummy_ver, machine, dummy_proc) = platform.uname()
    usb_out.write('Based on {} V{} for\n'.format(system, release))
    usb_out.write('{} with {} arch.\n\n'.format(node, machine))

def print_host_network_info(usb_out):
    '''Print information about the host network'''
    for network_interface in all_interfaces():
        usb_out.write(
            '{}\t{}\n'.format(
                network_interface,
                get_ip_address(network_interface)
            )
        )

def print_python_info(usb_out):
    '''Print information about the python installation'''
    usb_out.write(
        '\nRunning python V{}.{}\n\n'.format(
            sys.version_info[0], sys.version_info[1]
        )
    )

def print_sw_dep_diagnostics(usb_out, module_state):
    '''Print information to help diagnose printing issues due to known
    software dependencies'''
    usb_out.write('OS            = Ok\n')
    usb_out.write('Python        = Ok\n')
    usb_out.write('    struct    = Ok\n')

    pillow_msg = ok_if_found(module_state['pillow'])
    bitarray_msg = ok_if_found(module_state['bitarray'])
    qrcode_msg = ok_if_found(module_state['qrcode'])
    mysql_msg = ok_if_found(module_state['mysql'])
    pyqt4_msg = ok_if_found(module_state['pyqt4'])
    fclist_msg = ok_if_found(module_state['fclist'])

    usb_out.write('    Pillow    = {0}\n'.format(pillow_msg))
    usb_out.write('    bitarray  = {0}\n'.format(bitarray_msg))
    usb_out.write('    qrcode    = {0}\n'.format(qrcode_msg))
    usb_out.write('    MySQLdb   = {0}\n'.format(mysql_msg))
    usb_out.write('    PyQt4     = {0}\n'.format(pyqt4_msg))
    usb_out.write('    fclist    = {0}\n'.format(fclist_msg))

    cups_ok = False
    try:
        cups_ok = is_cups_present()
    except subprocess.CalledProcessError:
        pass

    if cups_ok:
        usb_out.write('CUPS driver   = Ok\n')
    else:
        usb_out.write('CUPS driver   = Missing\n')


def perform_connection_test(module_state):
    '''Performs a full Pipsta test (integration of Pipsta with Raspberry Pi)'''
    usb_out, dummy, printer_id = check_usb_connection()

    # Print, results
    usb_out.write('Welcome to Pipsta ({})\n'.format(printer_id))
    print_host_info(usb_out)
    print_host_network_info(usb_out)
    print_python_info(usb_out)
    print_sw_dep_diagnostics(usb_out, module_state)
    usb_out.write(FEED_PAST_CUTTER)

def instruct_pip_install():
    '''Prints instructions for installing pip'''
    print '''
To install python modules we uses pip.  To install pip try -
    sudo apt-get install python-pip python-dev
'''

def is_cups_present():
    '''Checks for a valid cups installation'''
    FNULL = open(os.devnull, 'w')
    cups_response = subprocess.check_output(
        ['dpkg', '-s', 'pipsta-cups-driver'],
        stderr = FNULL
    )
    version_ok = False
    installed_ok = False

    for line in cups_response.splitlines():
        fields = line.split(': ')
        if fields[0] == "Version":
            if fields[1] == '0.3.0':
                version_ok = True
        if fields[0] == 'Status':
            if 'ok' in fields[1] and 'installed' in fields[1]:
                installed_ok = True

    return version_ok and installed_ok

def check_system_files():
    '''If pipsta-cups-driver is installed, verify the version.  For older
    installations simply check for the present of some system files.'''
    try:
        if is_cups_present():
            return True

        print(
            'Please try installing (or reinstalling) pipsta-cups-driver v0.3.0'
        )
        return False

    except subprocess.CalledProcessError:
        old_blacklist_file = '/etc/modprobe.d/usblp_blacklist.conf'
        old_udev_file = '/etc/udev/rules.d/60-ablesystems-pyusb.rules'
        # Assuming this is pre-cups
        if os.path.isfile(old_blacklist_file) == False:
            print(
                'The system file usblp_blacklist.conf has not been installed, '
                'refer to installation guides or install pipsta-cups-driver '
                'v0.3.0'
            )
            return False
        if os.path.isfile(old_udev_file) == False:
            print(
                'The system file 60-ablesystems-pyusb.rules has not been '
                'installed, refer to installation guides or install '
                'pipsta-cups-driver v0.3.0'
            )
            return False

        return True

    return False

def print_wheezy_module_help(module_state):
    '''Print any useful (wheezy specific) help for module errors'''
    keys_of_worth = (
        'pip', 'pyusb', 'bitarray', 'pillow', 'qrcode', 'struct'
    )
    interested = {k:module_state[k] for k in keys_of_worth if k in module_state}

    if any(v == False for v in interested.itervalues()):
        print '''
The following instructions will install missing dependancies needed to run the
examples -'''

        if not interested['pip']:
            instruct_pip_install()
            print '    sudo apt-get install libjpeg8-dev'

        cmd_str = '    sudo pip install '
        if not interested['pyusb']:
            cmd_str += 'pyusb '
        if not interested['bitarray']:
            cmd_str += 'bitarray '
        if not interested['pillow']:
            cmd_str += 'Pillow '
        if not interested['qrcode']:
            cmd_str += 'qrcode '
        if not interested['struct']:
            cmd_str += 'struct '
        print cmd_str

    print ''

def print_jessie_module_help(module_state):
    '''Print any useful (jessie specific) help for module errors'''
    keys_of_worth = (
        'pyusb', 'bitarray', 'pillow', 'qrcode', 'struct'
    )
    interested = {k:module_state[k] for k in keys_of_worth if k in module_state}

    if any(v == False for v in interested.itervalues()):
        print '''
The following instructions will install missing dependancies needed to run the
examples -'''
        if not interested['struct'] or not interested['pyusb']:
            if not module_state['pip']:
                instruct_pip_install()

        cmd_str = '    sudo apt-get install '

        if not interested['pyusb']:
            cmd_str += 'python-usb '
        if not interested['bitarray']:
            cmd_str += 'python-bitarray '
        if not interested['pillow']:
            cmd_str += 'python-pil '
        if not interested['qrcode']:
            cmd_str += 'python-qrcode '

        print cmd_str

        if not interested['struct']:
            print '    sudo pip install struct'

        if not interested['pyusb']:
            print '''We need a newer version of pyusb than jessie ships
    sudo pip install --upgrade --pre pyusb'''

def print_general_module_help(module_state):
    '''Print help that is not dependant on the distribution'''
    if not module_state['mysql']:
        print '''
The following instructions will install missing dependancies for the database
examples -
    sudo apt-get install python-mysqldb
'''

    if not module_state['pyqt4'] or not module_state['fclist']:
        print ''
        print(
            'The following instructions will install missing dependancies '
            'for the optional examples_gui.py -'
        )

        if not module_state['pip']:
            instruct_pip_install()
        if not module_state['pyqt4']:
            print '    sudo apt-get install python-qt4 libffi-dev '
        if not module_state['fclist']:
            print '    sudo pip install fclist'

        print ''

    cups_ok = False
    try:
        cups_ok = is_cups_present()
    except subprocess.CalledProcessError:
        pass

    if not cups_ok:
        print 'It is recommended that you install pipsta-cups-driver'

def main():
    '''Main loop of application'''
    print 'Testing the Pipsta installation'

    if not python_check():
        print '''Install python2 versions 2.6 or greater. Try one of -
sudo apt-get install python2.6
or
sudo apt-get install python2.7
'''
        exit()

    platform_id = os_check()
    if platform_id == PLATFORM_UNKNOWN:
        sys.exit()

    try:
        module_state = {
            'pip': pip_check(),
            'pyusb': pyusb_check(),
            'bitarray': module_exists('bitarray'),
            'pillow': pillow_check(),
            'qrcode': module_exists('qrcode'),
            'mysql': module_exists('MySQLdb'),
            'struct': module_exists('struct'),
            'pyqt4': module_exists('PyQt4'),
            'fclist': module_exists('fclist'),
        }

        if platform_id == PLATFORM_DEBIAN_WHEEZY:
            print_wheezy_module_help(module_state)
        elif platform_id == PLATFORM_DEBIAN_JESSIE:
            print_jessie_module_help(module_state)

        print_general_module_help(module_state)

        # Check the system files are in place
        sys_ok = check_system_files()
        if module_state['pyusb'] and module_state['struct'] and sys_ok:
            print '''Performing a simple connection test
'''
            perform_connection_test(module_state)
            print ''

    except AssertionError as error:
        print error
    sys.exit()

# Ensure that BasicPrint is ran in a stand-alone fashion (as intended) and not
# imported as a module. Prevents accidental execution of code.
if __name__ == '__main__':
    main()
