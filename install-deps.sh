#!/bin/sh

apt-get update
apt-get install python-usb python-bitarray python-qrcode python-mysqldb
pip install --upgrade --pre pyusb
cp pipsta/Examples/system_scripts/usblp_blacklist.conf /etc/modprobe.d/
cp pipsta/Examples/system_scripts/60-ablesystems-pyusb.rules /etc/udev/rules.d/

modprobe -r usblp
service systemd-udev-trigger stop
service systemd-udev-trigger start

udevadm control --reload-rules
udevadm trigger

